package env

import (
	"github.com/joho/godotenv"
)

var env map[string]string


func Init() {
	envMap, _ := godotenv.Read(".env")
    env = envMap
}

func Get(key string) string {
    return env[key]
}