package db

import (
	"fmt"
	"petting/env"
	"petting/models/post"
	"petting/models/user"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var Db *gorm.DB

func Connect() {

	
	var (
		host     = env.Get("POSTGRES_HOST")
		port     = env.Get("POSTGRES_PORT")
		userDb     = env.Get("POSTGRES_USER")
		password = env.Get("POSTGRES_PASSWORD")
		dbname   = env.Get("POSTGRES_DB")
	)

	fmt.Println(host, port, userDb, password, dbname)

	dsn := "host=" + host + " user=" + userDb + " password=" + password + " dbname=" + dbname + " port=" + port + " sslmode=disable TimeZone=Asia/Yekaterinburg"
	createdDb, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})

	if err != nil {
		panic("failed to connect database")
	}

	Db = createdDb

	initModels()
}

func initModels() {
	user.Migrate(Db)
	post.Migrate(Db)
}
