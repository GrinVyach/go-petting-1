package router

import (
	"petting/services/user"

	"github.com/gofiber/fiber/v2"
)

var app *fiber.App

func Init() {
	app := fiber.New()

	app.Get("/", func(c *fiber.Ctx) error {
		return c.SendString("Hello, World!")
	})

	app.Post("/users", func(c *fiber.Ctx) error {
		return user.Create(c)
	})

	app.Listen(":3000")
}
