package user

import (
	"time"
	"gorm.io/gorm"
)

type User struct {
	gorm.Model
	ID        uint
	Name      string
	Email     *string
	Birthday  time.Time
	CreatedAt time.Time
	UpdatedAt time.Time
}

func Migrate(gormdb *gorm.DB) {
	gormdb.AutoMigrate(&User{})
}
