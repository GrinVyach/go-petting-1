package post

import (
	"time"
	"gorm.io/gorm"
	user "petting/models/user"
)

type Post struct {
	gorm.Model
	ID    uint
	Title string
	Text  string
	AuthorID int
  	Author   user.User

	CreatedAt time.Time
	UpdatedAt time.Time
}

func Migrate(gormdb *gorm.DB) {
	gormdb.AutoMigrate(&Post{})
}
