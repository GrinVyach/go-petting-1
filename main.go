package main

import (
	db "petting/database"
	"petting/env"
	"petting/router"
)

func main() {
	env.Init()
	db.Connect()
	router.Init()
}
