package user

import (
	"fmt"
	db "petting/database"
	"petting/models/user"

	"github.com/gofiber/fiber/v2"
)

func Create(c *fiber.Ctx) error {

	dto := new(user.User)

	err := c.BodyParser(dto)
	if err != nil {
		fmt.Println(4)
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Review your input", "data": err})
	}

	db.Db.Create(dto)

	return c.Status(201).JSON(fiber.Map{
		"status":  "success",
		"message": "User created successfully",
		"data":    dto,
	})
}
